# Challenge 1 - Views and Diagrams

## Latency and Scalability

This challenge has the ADD architecture, followed to solve the latency and scalability quality attributes.
The business problem to solve, is the following: 'CCS is the Colombian Vehicle Tracking Company. CCS is responsible for monitoring and tracking cargo vehicles, public transport vehicles and private vehicles including motorcycles. CCS installs sensors in vehicles so that it is possible at all times to know their location, speed and direction. If Particularly for cargo vehicles, you want to know the status of the cargo, cargo temperature, planned and unplanned stops, and accidents that each may have during the transportation of the cargo. In the case of trucks, each vehicle has an internal camera that records everything that happens inside the cabin. All vehicles have a panic button in case an emergency occurs. Drivers additionally have a mobile application, from which they can also activate an emergency call in case of require help due to mechanical problems or safety incidents. CCS has a central office in which all signals coming from both trucks and vehicles are consolidated. In this center we analyze all the data received and in case of detecting an anomalous situation or receiving an emergency signal, the authorities are informed respective, as well as to relief organizations and interested parties (owner of the truck). Vehicle owners can define, through a mobile application, the actions to be carried out from the center in the event of events that occur with the vehicle. For example, a truck owner can define a rule associated with an unplanned stop. If When the vehicle stops, a message must be sent to the truck owner indicating what is happening. Another example could be, if a public transport vehicle such as a taxi, is moving at a time when it should be stopped. In that case, you must send a message to owner. In the case of a panic button, a rule could be to call the authorities to report what is happening. CSS currently has 1,500 trucks, 5,000 vehicles and 3,000 motorcycles affiliated. However, CSS expects to increase its affiliated annually for the next 3 years. **Challenge:** Design the architecture of the CCS center, to ensure that all signals can be handled and corresponding actions taken quickly. In the event of an emergency signal, all programmed actions must be able to be executed in less than 2 seconds. Additionally, Up to 500 signals per second must be able to be processed, for up to 2 minute periods.'

Following the input, we used the following patterns for connector & component view:

- 3-tier
- pub / sub

3-tier was used to differentiate the parts of the system, followed by presentation tier, business tier and domain / persistence tier. The pub / sub pattern was used to solve the topics and different calls to the system keeping latency in the mind.

The views we created followed that architecture were:

- Functional view
- Sequence view
- Modules family view
- Modules specific view (emergency processor)
- Data diagram view
- Deployment view
- Concurrence view

## Members

- Joann Felipe Castellanos
- Nicolas Silva
- Andrés Guerrero
- Andrés Felipe Wilches Torres
